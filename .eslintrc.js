module.exports = {
  'env': {
    'es2021': true,
    'node': true,
  },
  'extends': [
    'google',
  ],
  'parserOptions': {
    'ecmaVersion': 'latest',
    'sourceType': 'module',
  },
  'rules': {
    'semi': ['error', 'never'],
    'linebreak-style': 'off',
    'max-len': ['error', 120],
    'require-jsdoc': 'off',
    'arrow-parens': 'off',
    'operator-linebreak': ['error', 'before'],
  },
}
