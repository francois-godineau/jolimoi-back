# Jolimoi test

## install / run
```
npm i
npm run dev
```

## objectives
convert arabic decimal to roman notation

## step 1
use xhr to retrieve result

## step 2 
use sse to broadcast result

## todo
- branch step1
- branch step2