import express from 'express'
import cookieParser from 'cookie-parser'
import logger from 'morgan'
import cors from 'cors'
import {handleValidationError} from '@/middlewares/validation.middleware'

import converterRouter from '@/routes/converter.route'

const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({extended: false}))
app.use(cookieParser())
app.use(cors())

app.use('/converter', converterRouter)

app.use(handleValidationError) // should be routes

export default app
