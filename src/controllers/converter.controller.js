import * as service from '@/services/converter.service'

async function arabicToRoman(req, res) {
  try {
    const {decimal} = req.body
    const result = await service.arabicToRoman(decimal)
    res.status(200).json(result)
  } catch (e) {
    res.status(400).json(e)
  }
}

export {
  arabicToRoman,
}
