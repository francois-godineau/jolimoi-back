import {isCelebrateError} from 'celebrate'

const handleValidationError = (error, req, res, next) => {
  if (isCelebrateError(error)) {
    let errorMessage = ''
    error.details.forEach((element) => errorMessage += element.message.replace('. ', ','))
    return res.status(400).json({
      message: errorMessage,
    })
  }
  return res.status(500).json(error.message)
}

export {
  handleValidationError,
}
