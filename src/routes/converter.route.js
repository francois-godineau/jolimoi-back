import {Router} from 'express'
import {celebrate} from 'celebrate'
import schema from '@/schemas/converter.schema'
import * as controller from '@/controllers/converter.controller'

const router = new Router()

router.route('/arabic-to-roman')
    .post(celebrate(schema.arabicToRomanRequestSchema), controller.arabicToRoman)

export default router
