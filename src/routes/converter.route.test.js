import {expect} from 'chai'
import express from 'express'
import request from 'supertest'
import {handleValidationError} from '@/middlewares/validation.middleware'

import routes from '@/routes/converter.route'

const app = express()
app.use(express.json())
app.use('/converter', routes)
app.use(handleValidationError)


describe('# converter.route.test', () => {
  it('should POST /converter/arabic-to-roman and get 200', async () => {
    const payload = {decimal: 22}
    const res = await request(app)
        .post('/converter/arabic-to-roman')
        .send(payload)
        .expect('Content-Type', /json/)
        .expect(200)
    expect(res.body).to.equal('XXII')
  })
  it('should POST /converter/arabic-to-roman and get 400 wrong type', async () => {
    const payload = {decimal: 'TYU'}
    const res = await request(app)
        .post('/converter/arabic-to-roman')
        .send(payload)
        .expect('Content-Type', /json/)
        .expect(400)
    expect(res.body).to.have.property('message', '"decimal" must be a number')
  })
  it('should POST /converter/arabic-to-roman and get 400 param required', async () => {
    const payload = {decimals: 'TYU'}
    const res = await request(app)
        .post('/converter/arabic-to-roman')
        .send(payload)
        .expect('Content-Type', /json/)
        .expect(400)
    expect(res.body).to.have.property('message', '"decimal" is required')
  })
})
