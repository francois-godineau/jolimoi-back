import {Joi, Segments} from 'celebrate'

const arabicToRomanRequestSchema = {
  [Segments.BODY]: Joi.object().keys({
    decimal: Joi.number().min(0).max(100).required(),
  }),
}

export default {
  arabicToRomanRequestSchema,
}
