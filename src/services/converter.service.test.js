import {expect} from 'chai'
import {arabicToRoman} from '@/services/converter.service'

describe('# converter.service.test', () => {
  it('should convert from arabic to roman', () => {
    const tests = {
      1: 'I',
      4: 'IV',
      9: 'IX',
      11: 'XI',
      27: 'XXVII',
      49: 'XLIX',
      52: 'LII',
    }
    Object.keys(tests).map(decimal => {
      const res = arabicToRoman(decimal)
      expect(res).to.equal(tests[decimal], 'wrong roman notation')
    })
  })
})
